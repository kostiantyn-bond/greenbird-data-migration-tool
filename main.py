from time import sleep

from influxdb import InfluxDBClient
import pandas as pd
import os
import json
from google.cloud import pubsub, pubsub_v1
from google.cloud.pubsub import types


def callback(future):
    message_id = future.result()
    # print("Published to PUBSUB message: {}".format(message_id))


def main():
    # For neuron we have raw mqtt message:
    # {"status":4,"value":82,"value2":532,"rssi":110,"battery":3,"topic":"neuron/2800180/1/20007123"}
    # with topic attribute: neuron/2800180/1/20007123.
    project_id = os.getenv('GOOGLE_CLOUD_PROJECT', "plasma-elf-257710")
    topic_name = "mqtt-sensors"
    topic = 'projects/{}/topics/{}'.format(project_id, topic_name)

    # Configure the batch to publish as soon as there is ten messages,
    # one kilobyte of data, or one second has passed.
    batch_settings = pubsub_v1.types.BatchSettings(
        max_messages=100,  # default 100
        max_bytes=1024 * 1024 * 10,  # 10 MB
        max_latency=120,  # default 120 s
    )

    publish_client = pubsub.PublisherClient(batch_settings=batch_settings)

    client = InfluxDBClient('localhost', 8086, '', '', 'alldata')
    chunk_size = 100000000

    sensor_names = [
        # 'AirThings\/n54-apoint',
        # 'AirThings\/f13\/meetingroom\/northwest',
        # 'Dalegata137\/HF\/P0',
        # 'WE122\/433_05_02\/RE004',
        # 'WE122\/433_05_02\/RE017',
        # 'WE122\/433_05_02\/RE016',
        # 'WE122\/433_05_02\/RE003',
        # 'WE122\/433_05_02\/RE004',
        # 'WE122\/433_05_02\/RE005',
        # 'WE122\/433_05_02\/RE018', <--
        # 'AirThings\/f13\/meetingroom\/southeast',
        # 'HAN\/FG\/F13\/663267',
        # 'WE122\/433_05_02\/RE020',
        # 'Dalegata137\/HF\/KWH1_7',
        # 'Dalegata137\/HF\/KWH1_1',
        # 'Dalegata137\/HF\/KWH1_4',
        # 'Dalegata137\/HF\/KWH1_6',
        # 'HAN\/FG\/F13\/833055',
        'Dalegata137\/HF\/KWH1_5'
    ]

    for sensor_name in sensor_names:

        sensor_fields = ['humidity_airthings', 'light_airthings', 'radon_short_airthings', 'radon_long_airthings',
                         'temperature_airthings', 'pressure_airthings', 'co2_airthings', 'voc_airthings', 'value',
                         'value2', 'topic', 'battery', 'status', 'rssi', 'U1', 'I1', 'P1', 'Pr1', 'Pf1', 'U2', 'I2',
                         'P2', 'Pr2', 'Pf2', 'U3', 'I3', 'P3', 'Pr3', 'Pf3', 'P', 'Prl', 'Prc', 'Prc3', 'Pf3', 'cos',
                         'Pf', 'Hz', 'U12', 'U23', 'U31', 'ThdU1', 'ThdU2', 'ThdU3', 'cos', 'ThdI1', 'ThdI2', 'ThdI3',
                         'Ep', 'Erlp', "\"In\"", 'Ercp', 'current', 'voltage', 'consumption', 'voltage']
        restrictions = ["time >= '2020-02-10 09:04:40'", "time < '2020-06-16 00:00:00'"]

        time_restrictions = " AND " + " AND ".join(restrictions)
        if time_restrictions is " AND ":
            time_restrictions = ""

        q = "select {} from mqtt_consumer_alldata WHERE topic =~ /.*{}/ {} ORDER BY time ASC LIMIT {}".format(
            ",".join(sensor_fields), sensor_name, time_restrictions, chunk_size)
        df = pd.DataFrame(client.query(q, chunked=True, chunk_size=chunk_size).get_points())

        # end_date = df.sort_values(by='time', ascending=False).head(5)
        # start_date = df.sort_values(by='time', ascending=True).head(5)

        for index, row in df.iterrows():
            # if index % 500 == 0:
            #     sleep(2)

            data = {s: row[s.replace('"', '')] for s in sensor_fields if row[s.replace('"', '')] is not None}
            customPublishTime = row['time']
            json_data = json.dumps(data)
            print("Publish {} message: {}, customPublishTime: {}".format(index, json_data, customPublishTime))
            future = publish_client.publish(topic, data=json_data.encode("utf-8"), customPublishTime=customPublishTime,
                                            customTopic=data['topic'])
            # we could track if message successfully delivered by checking - future
            future.add_done_callback(callback)
        print("Successfully finished import influxdb history for sensor: {}".format(sensor_name))


if __name__ == "__main__":
    main()
