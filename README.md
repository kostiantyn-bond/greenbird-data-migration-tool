## Description
Simple script that will search single neuron sensor from local influxdb and publish this message to Google PubSub topic: mqtt-sensors

### Params
sensor_name - name of the registered sensor that need to fetch historic data
sensor_fields - list of field that original mqtt message requires. For neuron it looks like:
```json
{"status":4,"value":82,"value2":532,"rssi":110,"battery":3,"topic":"neuron/2800180/1/20007123"}
```

### Run migration
1. Define env variable GOOGLE_APPLICATION_CREDENTIALS point to credential file
```shell script
GOOGLE_APPLICATION_CREDENTIALS=/Users/XYZ/...
```
2. Run the script
```shell script
python3 main.py
```

### TODO:
1. Define dynamic custom WHERE clause to InfluxQL